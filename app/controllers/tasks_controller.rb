# encoding: UTF-8

class TasksController < ApplicationController
  before_filter :authenticate_user!

  # GET /tasks
  # GET /tasks.json
  def index
    if params[:category]
      params[:category] == 'clear' ? session[:category_selected] = nil : session[:category_selected] = params[:category]
    end

    @category_selected = session[:category_selected]

    unless @category_selected
      @tasks = Task.where("user_id = ?", current_user.id).all
    else
      if @category_selected == "none"
        @tasks = Task.where("user_id = ? AND category_id IS NULL", current_user.id).all
      else
        @tasks = Task.where("user_id = ? AND category_id = ?", current_user.id,@category_selected).all
      end
    end

    @expand_id = session[:expand_id]

    @temp = Array.new
    @tasks.each do |task|
      if task.is_closed?
        if task.diff_between_not_to_date_of_completion < 1
          @temp.push task
        end
      else
        @temp.push task
      end
    end
    @tasks = @temp

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @tasks }
    end
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
    @task = Task.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @task }
    end
  end

  # GET /tasks/new
  # GET /tasks/new.json
  def new
    @task = Task.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @task }
    end
  end

  # GET /tasks/1/edit
  def edit
    @task = Task.find(params[:id])
  end

  # POST /tasks
  # POST /tasks.json
  def create
    @task = Task.new(params[:task])

    session[:expand_id] = @task.id

    respond_to do |format|
      if @task.save
        format.html { redirect_to tasks_path, :notice => 'A tarefa foi criada com sucesso.' }
        format.json { render :json => @task, :status => :created, :location => @task }
      else
        format.html { render :action => "new" }
        format.json { render :json => @task.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /tasks/1
  # PUT /tasks/1.json
  def update
    @task = Task.find(params[:id])

    session[:expand_id] = @task.id

    respond_to do |format|
      if @task.update_attributes(params[:task])
        format.html { redirect_to tasks_path, :notice => 'A tarefa foi atualizada com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @task.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @task = Task.find(params[:id])
    @task.destroy

    respond_to do |format|
      format.html { redirect_to tasks_url, :notice => 'A tarefa foi removida com sucesso.' }
      format.json { head :no_content }
    end
  end

  def reopen
    @task = Task.find(params[:id])
    @task.status = "open"

    session[:expand_id] = @task.id

    respond_to do |format|
      if @task.save
        format.html { redirect_to tasks_path, :notice => 'A tarefa selecionada foi reaberta.' }
        format.json { head :no_content }
      else
        format.html { redirect_to tasks_path, :notice => 'A tarefa selecionada não foi reaberta.' }
        format.json { render :json => @task.errors, :status => :unprocessable_entity }
      end
    end
  end

  def close
    @task = Task.find(params[:id])
    @task.date_of_completion = DateTime.now
    @task.status = "closed"

    session[:expand_id] = @task.id

    respond_to do |format|
      if @task.save
        format.html { redirect_to tasks_path, :notice => 'A tarefa selecionada foi finalizada.' }
        format.json { head :no_content }
      else
        format.html { redirect_to tasks_path, :notice => 'A tarefa selecionada não foi finalizada.' }
        format.json { render :json => @task.errors, :status => :unprocessable_entity }
      end
    end
  end

  def hold
    @task = Task.find(params[:id])
    @task.start_date = DateTime.now
    @task.status = "hold"

    session[:expand_id] = @task.id

    respond_to do |format|
      if @task.save
        format.html { redirect_to tasks_path, :notice => 'A tarefa selecionada foi posta em execução.' }
        format.json { head :no_content }
      else
        format.html { redirect_to tasks_path, :notice => 'A tarefa selecionada não foi posta em execução.' }
        format.json { render :json => @task.errors, :status => :unprocessable_entity }
      end
    end
  end

  # GET /tasks
  # GET /tasks.json
  def archive
    @tasks = Task.where("user_id = ? AND status = 'closed'", current_user.id).all
    @expand_id = session[:expand_id]

    @temp = Array.new
    @tasks.each do |task|
      if task.diff_between_not_to_date_of_completion >= 1
        @temp.push task
      end
    end
    @tasks = @temp

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @tasks }
    end
  end
end
