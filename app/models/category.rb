class Category < ActiveRecord::Base
  has_many :tasks
  belongs_to :user

  attr_accessible :title, :user_id
end
