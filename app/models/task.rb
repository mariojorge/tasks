include ActionView::Helpers::DateHelper
require 'time_diff'

class Task < ActiveRecord::Base
  belongs_to :user
  belongs_to :category

  attr_accessible :description, :status, :title, :user_id, :category_id

  validates_presence_of :title, :description

  after_create :set_task_open

  def set_task_open
    self.update_attributes :status => :open
  end

  def is_open?
    self.status == 'open' ? true : false
  end

  def is_closed?
    self.status == 'closed' ? true : false
  end

  def is_hold?
    self.status == 'hold' ? true : false
  end

  def time_spent_in_words
    distance_of_time_in_words self.date_of_completion, self.start_date, :true
  end

  def diff_between_not_to_date_of_completion
    if self.is_closed?
      Time.diff(Time.now, self.date_of_completion)[:day]
    else
      0
    end
  end
end
