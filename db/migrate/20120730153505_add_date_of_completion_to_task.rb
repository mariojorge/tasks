class AddDateOfCompletionToTask < ActiveRecord::Migration
  def change
    add_column :tasks, :date_of_completion, :datetime
  end
end
