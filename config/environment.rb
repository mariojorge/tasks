# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
Tasks::Application.initialize!

Devise::SessionsController.layout "login"
Devise::RegistrationsController.layout proc{ |controller| user_signed_in? ? "application" : "login" }
Devise::ConfirmationsController.layout "login"
Devise::UnlocksController.layout "login"            
Devise::PasswordsController.layout "login"        
